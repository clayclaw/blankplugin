<#if shadowjar>
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
</#if>
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.net.URI

group = "${group}"
version = "${version}"

val kotlinVersion = "1.5.20"

plugins {
    java
    kotlin("jvm") version "1.5.20"
    <#if shadowjar>
        id("com.github.johnrengelman.shadow") version "7.0.0"
    </#if>
    <#if bintray>
        `maven-publish`
        id("com.jfrog.bintray") version "1.8.4"
    </#if>
}

java {
    sourceCompatibility = JavaVersion.VERSION_16
    targetCompatibility = JavaVersion.VERSION_16
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "16"
    kotlinOptions.freeCompilerArgs = listOf("-Xjvm-default=compatibility")
}

repositories {
    mavenCentral()
    maven { url = URI.create("https://hub.spigotmc.org/nexus/content/repositories/snapshots") }
    maven { url = URI.create("https://oss.sonatype.org/content/repositories/snapshots/") }
}

dependencies {
    compileOnly("dev.reactant:reactant:0.2.3")
    compileOnly("org.spigotmc:spigot-api:1.17.1-R0.1-SNAPSHOT")

    // Load jar libraries from "libs" folder
    compileOnly(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    /** The external library you would like to use */
    /** implementation("...")    */
}


val sourcesJar by tasks.registering(Jar::class) {
    dependsOn(JavaPlugin.CLASSES_TASK_NAME)
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

<#if shadowjar>
val shadowJar = (tasks["shadowJar"] as ShadowJar).apply {
	exclude("kotlin*")
    //relocate("org.from.package", "org.target.package")
}
</#if>

<#if autodeploy>
val deployPlugin by tasks.registering(Copy::class) {
    dependsOn(shadowJar)
    System.getenv("PLUGIN_DEPLOY_PATH")?.let {
        from(shadowJar)
        into(it)
    }
}
</#if>

val build = (tasks["build"] as Task).apply {
    arrayOf(
            sourcesJar
            <#if shadowjar>
                , shadowJar
            </#if>
            <#if autodeploy>
                , deployPlugin
            </#if>
    ).forEach { dependsOn(it) }
}

<#if bintray>
    publishing {
        publications {
            create<MavenPublication>("maven") {
                from(components["java"])
                artifact(sourcesJar.get())
                artifact(shadowJar)

                groupId = group.toString()
                artifactId = project.name
                version = version
            }
        }
    }

    bintray {
        user = System.getenv("BINTRAY_USER")
        key = System.getenv("BINTRAY_KEY")
        setPublications("maven")
        publish = true
        override = true
        pkg.apply {
            repo = "${bintrayRepo}"
            name = project.name
            setLicenses("GPL-3.0")
            vcsUrl = "${vcsUrl}"
        }
    }
</#if>
